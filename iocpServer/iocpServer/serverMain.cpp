#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#pragma comment(lib, "ws2_32")
#include <WS2tcpip.h>
//#include <winsock2.h>

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <vector>
#include <list>
#include <mutex>

#include "protocol.h"


using std::map;
using std::vector;
using std::list;
using std::cout;


// 소켓 정보 저장을 위한 구조체
struct SOCKETINFO
{
	OVERLAPPED overlapped;
	SOCKET sock;
	char buf[MAX_BUFFER];
	WSABUF wsabuf;
	bool is_recv;
	int id;

};

struct ROOM_INFO
{
	int roomNumber;
	short board[17][17];
	int hostID, guestID;
	short stoneCount;
};

map <int, SOCKETINFO*> clients;
HANDLE iocp;
int new_user_id;
vector<ROOM_INFO> gameRoomVector;	// 게임룸 구조체들 담을 컨테이너
list<int> roomNumberList;	// 방 번호 설정


// 작업자 스레드 함수
DWORD WINAPI WorkerThread(LPVOID arg);
// 오류 출력 함수
void err_quit(char *msg);
void err_display(char *msg);


void send_packet(int id, void* buf) // WSASend 처리
{
	char* packet = reinterpret_cast<char*>(buf);
	int packet_size = packet[0];
	SOCKETINFO *sendInfo = new SOCKETINFO;
	memset(sendInfo, 0x00, sizeof(SOCKETINFO));
	sendInfo->is_recv = false;
	memcpy(sendInfo->buf, packet, packet_size);
	sendInfo->wsabuf.buf = sendInfo->buf;
	sendInfo->wsabuf.len = packet_size;

	WSASend(clients[id]->sock, &sendInfo->wsabuf, 1, 0, 0, &sendInfo->overlapped, 0);
}

void send_login_ok_packet(int id) // 클라이언트 접속 확인 패킷 send
{
	sc_packet_login_ok packet;
	packet.size = sizeof(packet);
	packet.type = SC_LOGIN_OK;
	packet.id = id;
	packet.state = STATE_FIND_MATCH;
	send_packet(id, &packet);
}

void send_room_info_packet(int roomHost, int roomGuest, int roomNum, int receiver) // 방 정보 패킷 send
{
	sc_packet_room_info packet;
	packet.size = sizeof(packet);
	packet.type = SC_ROOM_INFO;
	packet.hostID = roomHost;
	packet.guestID = roomGuest;
	packet.roomNumber = roomNum;

	send_packet(receiver, &packet);
}

void send_join_room_ok_packet(int roomHost, int joiner, int receiver) // 방입장 패킷 send
{
	sc_packet_join_room_ok packet;
	packet.size = sizeof(packet);
	packet.type = SC_JOIN_ROOM_OK;
	packet.hostID = roomHost;
	packet.joinID = joiner;

	send_packet(receiver, &packet);
}

void send_chat_packet(int sender, int receiver, char * chat) // 채팅 패킷 send
{
	sc_packet_chat packet;
	packet.size = sizeof(packet);
	packet.type = SC_CHAT;
	packet.sender = sender;
	packet.receiver = receiver;
	memcpy(packet.chat, chat, CHAT_BUFFER);

	send_packet(receiver, &packet);
}

void send_put_stone_packet(int sender, int receiver, int stoneType, short x, short y)
{
	sc_packet_put_stone packet;
	packet.size = sizeof(packet);
	packet.type = SC_PUT_STONE;
	packet.stoneType = stoneType;
	packet.senderID = sender;
	packet.receiverID = receiver;
	packet.x = x;
	packet.y = y;

	send_packet(receiver, &packet);
}

void send_game_result_packet(int winnerID, int loserID)
{
	sc_packet_game_result packet;
	packet.size = sizeof(packet);
	packet.type = SC_GAME_RESULT;
	packet.winnerID = winnerID;
	packet.loserID = loserID;

	send_packet(winnerID, &packet);
	send_packet(loserID, &packet);
}

void send_remove_room_packet(int hostID)
{
	sc_packet_remove_room packet;
	packet.size = sizeof(packet);
	packet.type = SC_REMOVE_ROOM;
	packet.hostID = hostID;

	for (auto &cl : clients) {
		send_packet(cl.first, &packet);
	}
}

void send_leave_room_packet(int leaverID)
{
	sc_packet_leave_room packet;
	packet.size = sizeof(packet);
	packet.type = SC_LEAVE_ROOM;
	packet.leaverID = leaverID;

	for (auto &cl : clients) {
		send_packet(cl.first, &packet);
	}
}

short judgeOmok(short board[][17])
{
	for (int y = 0; y < 17; ++y) {
		for (int x = 2; x < 15; ++x) { // 가로 방향 체크
			if (board[y][x - 2] == STONE_WHITE && board[y][x - 1] == STONE_WHITE
				&& board[y][x] == STONE_WHITE && board[y][x + 2] == STONE_WHITE
				&& board[y][x + 2] == STONE_WHITE) {
				return STONE_WHITE;
			}
			else if (board[y][x - 2] == STONE_BLACK && board[y][x - 1] == STONE_BLACK
				&& board[y][x] == STONE_BLACK && board[y][x + 2] == STONE_BLACK
				&& board[y][x + 2] == STONE_BLACK) {
				return STONE_BLACK;
			}
		}
	}
	for (int y = 2; y < 15; ++y) {
		for (int x = 0; x < 17; ++x) { // 세로 방향 체크
			if (board[y - 2][x] == STONE_WHITE && board[y - 1][x] == STONE_WHITE
				&& board[y][x] == STONE_WHITE && board[y + 1][x] == STONE_WHITE
				&& board[y + 2][x] == STONE_WHITE) {
				return STONE_WHITE;
			}
			else if (board[y - 2][x] == STONE_BLACK && board[y - 1][x] == STONE_BLACK
				&& board[y][x] == STONE_BLACK && board[y + 1][x] == STONE_BLACK
				&& board[y + 2][x] == STONE_BLACK) {
				return STONE_BLACK;
			}
		}
	}
	for (int y = 2; y < 15; ++y) {
		for (int x = 2; x < 15; ++x) { // 대각선 1
			if (board[y - 2][x - 2] == STONE_WHITE && board[y - 1][x - 1] == STONE_WHITE
				&& board[y][x] == STONE_WHITE && board[y + 1][x + 1] == STONE_WHITE
				&& board[y + 2][x + 2] == STONE_WHITE) {
				return STONE_WHITE;
			}
			else if (board[y - 2][x - 2] == STONE_BLACK && board[y - 1][x - 1] == STONE_BLACK
				&& board[y][x] == STONE_BLACK && board[y + 1][x + 1] == STONE_BLACK
				&& board[y + 2][x + 2] == STONE_BLACK) {
				return STONE_BLACK;
			}

			else if (board[y - 2][x + 2] == STONE_WHITE && board[y - 1][x + 1] == STONE_WHITE
				&& board[y][x] == STONE_WHITE && board[y + 1][x - 1] == STONE_WHITE
				&& board[y + 2][x - 2] == STONE_WHITE) {
				return STONE_WHITE;
			}
			else if (board[y - 2][x + 2] == STONE_BLACK && board[y - 1][x + 1] == STONE_BLACK
				&& board[y][x] == STONE_BLACK && board[y + 1][x - 1] == STONE_BLACK
				&& board[y + 2][x - 2] == STONE_BLACK) {
				return STONE_BLACK;
			}
		}
	}

	return STONE_NONE;
}

void ProcessPutStone(cs_packet_put_stone * packet)	// 오목 돌 놓기 메시지 처리 함수
{
	int sender = packet->senderID;
	int receiver = packet->receiverID;
	short stoneType = packet->stoneType;
	short x, y;
	x = packet->x;
	y = packet->y;
	for (auto &room : gameRoomVector) {
		if (sender == room.hostID || sender == room.guestID) {
			if (room.board[y][x] == STONE_NONE) {
				room.board[y][x] = stoneType;
				cout << x << ", " << y << "\n";
				send_put_stone_packet(sender, receiver, stoneType, x, y);
				send_put_stone_packet(sender, sender, stoneType, x, y);
				short ret = judgeOmok(room.board);
				if (STONE_WHITE == ret) {
					cout << "white win \n";
					send_game_result_packet(room.hostID, room.guestID);
				}
				else if (STONE_BLACK == ret) {
					cout << "black win \n";
					send_game_result_packet(room.guestID, room.hostID);
				}
			}
			// 이제 여기에 오목 승부체크

		}
	}
}

void ProcessPacket(int id, void* buf) // 모든 패킷 처리
{
	char* packet = reinterpret_cast<char*>(buf);

	switch (packet[1]) {
	case CS_MAKE_ROOM:	// 방 생성 메시지 처리
	{
		cs_packet_make_room *make_room_packet = reinterpret_cast<cs_packet_make_room *>(buf);

		int roomHostID = make_room_packet->id;
		ROOM_INFO gameRoom{};
		gameRoom.board[17][17] = { 0, };
		gameRoom.hostID = make_room_packet->id;
		gameRoom.roomNumber = roomNumberList.front();
		gameRoom.guestID = -1;
		roomNumberList.pop_front();
		gameRoomVector.emplace_back(gameRoom);
		for (auto &cl : clients) {
			send_room_info_packet(roomHostID, gameRoom.guestID, gameRoom.roomNumber, cl.first);
		}
		break;
	}
	case CS_JOIN_ROOM: // 방 입장 메시지 처리
	{
		cs_packet_join_room *join_room_packet = reinterpret_cast<cs_packet_join_room *>(buf);
		int roomHostID = join_room_packet->hostID;
		int joinerID = join_room_packet->joinID;
		for (auto &room : gameRoomVector) {
			if (room.hostID == roomHostID) {
				room.guestID = joinerID;
				break;
			}
		}
		for (auto &cl : clients) {
			send_join_room_ok_packet(roomHostID, joinerID, cl.first);
		}
		break;
	}
	case CS_PUT_STONE: // 돌 놓기 처린
	{
		cs_packet_put_stone *put_stone_packet = reinterpret_cast<cs_packet_put_stone *>(buf);
		ProcessPutStone(put_stone_packet);
		break;
	}
	case CS_CHAT: // 채팅 처리
	{
		cs_packet_chat *chat_packet = reinterpret_cast<cs_packet_chat *>(buf);
		char chat[CHAT_BUFFER];
		memcpy(chat, chat_packet->chat, CHAT_BUFFER);
		int sender = chat_packet->senderID;
		for (auto &room : gameRoomVector) {
			if (room.hostID == sender) {	//호스트가 채팅보낼때
				int receiver = room.guestID;
				send_chat_packet(sender, receiver, chat);
				break;
			}
			else if (room.guestID == sender) { // 게스트가 채팅보낼때
				int receiver = room.hostID;
				send_chat_packet(sender, receiver, chat);
				break;
			}
		}

		break;
	}

	}
}



int main(int argc, char *argv[])
{
	for (int i = 1; i < 21; ++i) {	// 리스트에 방번호 넣고 방 만들때마다 pop해서 사용
		roomNumberList.emplace_back(i);
	}


	int retval;

	// 윈속 초기화
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) return 1;

	// 입출력 완료 포트 생성
	HANDLE hcp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (hcp == NULL) return 1;

	// CPU 개수 확인
	SYSTEM_INFO si;
	GetSystemInfo(&si);

	// (CPU 개수 * 2)개의 작업자 스레드 생성
	HANDLE hThread;
	for (int i = 0; i < (int)si.dwNumberOfProcessors * 2; i++) {
		hThread = CreateThread(NULL, 0, WorkerThread, hcp, 0, NULL);
		if (hThread == NULL) return 1;
		CloseHandle(hThread);
	}

	// socket()
	SOCKET listen_sock = socket(AF_INET, SOCK_STREAM, 0);
	//if (listen_sock == INVALID_SOCKET) err_quit("socket()");

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(SERVER_PORT);
	bind(listen_sock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	//if (retval == SOCKET_ERROR) err_quit("bind()");

	// listen()
	retval = listen(listen_sock, 5);
	//if (retval == SOCKET_ERROR) err_quit("listen()");

	// 데이터 통신에 사용할 변수
	SOCKET client_sock;
	SOCKADDR_IN clientaddr;
	int addrlen;
	DWORD flags;

	addrlen = sizeof(SOCKADDR_IN);
	memset(&clientaddr, 0, addrlen);


	while (1) {
		// accept()

		client_sock = accept(listen_sock, (SOCKADDR *)&clientaddr, &addrlen);
		if (client_sock == INVALID_SOCKET) {
			//err_display("accept()");
			break;
		}
		printf("[TCP 서버] 클라이언트 접속: IP 주소=%s, 포트 번호=%d\n",
			inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));



		// 소켓과 입출력 완료 포트 연결
		CreateIoCompletionPort((HANDLE)client_sock, hcp, client_sock, 0);

		// 소켓 정보 구조체 할당
		int user_id = new_user_id++;
		clients[user_id] = new SOCKETINFO;
		//memset(&clients[user_id], 0, sizeof(struct SOCKETINFO));
		clients[user_id]->sock = client_sock;
		clients[user_id]->wsabuf.len = MAX_BUFFER;
		clients[user_id]->wsabuf.buf = clients[user_id]->buf;
		clients[user_id]->is_recv = true;
		clients[user_id]->id = user_id;

		send_login_ok_packet(user_id);
		for (auto &room : gameRoomVector) {
			send_room_info_packet(room.hostID, room.guestID, room.roomNumber, user_id);
		}

		memset(&clients[user_id]->overlapped, 0, sizeof(clients[user_id]->overlapped));
		flags = 0;
		WSARecv(client_sock, &clients[user_id]->wsabuf, 1, NULL,
			&flags, &(clients[user_id]->overlapped), NULL);

	}

	// 윈속 종료
	WSACleanup();
	return 0;
}

// 작업자 스레드 함수
DWORD WINAPI WorkerThread(LPVOID arg)
{
	int retval;
	HANDLE hcp = (HANDLE)arg;

	while (1) {
		// 비동기 입출력 완료 기다리기
		DWORD cbTransferred;
		SOCKET client_sock;
		SOCKETINFO *ptr;
		retval = GetQueuedCompletionStatus(hcp, &cbTransferred,
			(PULONG_PTR)(LPDWORD)&client_sock, (LPOVERLAPPED *)&ptr, INFINITE);
		int key = ptr->id;


		if (cbTransferred == 0) { // 클라이언트에서 소켓접속종료됬을때
			closesocket(client_sock);
			int id = clients[key]->id;
			clients.erase(key);

			int index = 0;
			auto tempVec = gameRoomVector;
			for (auto &room : gameRoomVector) {
				if (id == room.hostID) {
					gameRoomVector.erase(gameRoomVector.begin() + index);
					send_remove_room_packet(id);
					break;
				}
				else if (id == room.guestID) {
					gameRoomVector[index].guestID = -1;
					send_leave_room_packet(id);
					break;
				}
				++index;
			}
			continue;
		}

		if (ptr->is_recv == true) {
			cout << "from client[" << key << "\n";
			cout << ptr->buf << " (" << cbTransferred << ") bytes \n";

			// 패킷 처리
			ProcessPacket(key, ptr->buf);


			DWORD flags = 0;
			memset(&ptr->overlapped, 0x00, sizeof(OVERLAPPED));
			WSARecv(client_sock, &ptr->wsabuf, 1, 0, &flags, &ptr->overlapped, 0);
		}


	}
	return 0;
}

// 소켓 함수 오류 출력 후 종료
void err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	//MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

// 소켓 함수 오류 출력
void err_display(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char *)lpMsgBuf);
	LocalFree(lpMsgBuf);
}

