#pragma once


// define
#define CHAT_BUFFER	100
#define STONE_NONE		0
#define STONE_WHITE		1
#define STONE_BLACK		2
	

// define state
#define STATE_FIND_MATCH	0
#define STATE_IN_GAMEROOM	1
#define STATE_PLAYING_GAME	2
#define STATE_END_GAME		3

// server
#define MAX_BUFFER 1024
#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 9000

// sever to client
#define SC_LOGIN_OK			1
#define SC_ROOM_INFO		2
#define SC_JOIN_ROOM_OK		3
#define SC_CHAT				4
#define SC_PUT_STONE		5
#define SC_GAME_RESULT		6
#define SC_REMOVE_ROOM		7
#define SC_LEAVE_ROOM		8

// client to server 
#define CS_MAKE_ROOM		11
#define CS_JOIN_ROOM		12
#define CS_CHAT				13
#define CS_PUT_STONE		14


// server to client packet
#pragma pack(push ,1)

struct sc_packet_login_ok
{
	char size;
	char type;
	int id;
	short state;
};

struct sc_packet_room_info
{
	char size;
	char type;
	int hostID;
	int guestID;
	int roomNumber;
};

struct sc_packet_join_room_ok
{
	char size;
	char type;
	int hostID;
	int joinID;
};

struct sc_packet_chat
{
	char size;
	char type;
	char sender;
	char receiver;
	char chat[CHAT_BUFFER];
};

struct sc_packet_put_stone
{
	char size;
	char type;
	int senderID;
	int receiverID;
	short stoneType;
	short x, y;
};

struct sc_packet_game_result
{
	char size;
	char type;
	int winnerID;
	int loserID;
};

struct sc_packet_remove_room
{
	char size;
	char type;
	int hostID;
};

struct sc_packet_leave_room
{
	char size;
	char type;
	int leaverID;
};

// client to server packet
struct cs_packet_make_room
{
	char size;
	char type;
	int id;
};

struct cs_packet_join_room
{
	char size;
	char type;
	int joinID;
	int hostID;
};

struct cs_packet_chat
{
	char size;
	char type;
	int senderID;
	int receiverID;
	char chat[CHAT_BUFFER];
};

struct cs_packet_put_stone
{
	char size;
	char type;
	int senderID;
	int receiverID;
	short stoneType;
	short x, y;
};

#pragma pack (pop) 