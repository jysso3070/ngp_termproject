#pragma once
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>


#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WS2tcpip.h> 
#pragma comment(lib, "Ws2_32.lib") //라이브러리
#include "..\..\iocpServer\iocpServer\protocol.h"
using namespace std;

// define
#define WINDOWSize_X	600
#define WINDOWSize_Y	600
#define WM_SOCKET		WM_USER + 1
#define RESULT_WIN		1
#define RESULT_LOSE		2

// struct
struct PLAYER_INFO {
	int id;
	short state;
};

struct ROOM_INFO
{
	int number;
	short board[17][17];
	int hostID, guestID;
	short stoneCount;
};



