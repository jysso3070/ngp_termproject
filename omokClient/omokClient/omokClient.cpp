﻿// omokClient.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "omokClient.h"
#include "globals.h"

//#pragma comment(linker,"/entry:WinMainCRTStartup /subsystem:console")

#define MAX_LOADSTRING 100

// 전역변수
WSABUF recv_buf;
char buffer[MAX_BUFFER];

PLAYER_INFO myInfo{};
vector<ROOM_INFO> gameRoomVector;	// 게임룸 구조체들 담을 컨테이너
ROOM_INFO myGameInfo;
short gameResultFlag = 0;

void ReadBuffer(SOCKET sock);
void PacketProccess(void * buf);

// 채팅
vector<string> Chatting;
char str[MAX_STRING]{};
int strCount;
bool DrawTurn = FALSE;

void ReadBuffer(SOCKET sock) // 수신버퍼로 들어온 다수의 패킷 읽어들이기
{
	int in_packet_size = 0;
	int saved_packet_size = 0;

	DWORD iobyte, ioflag = 0;
	WSARecv(sock, &recv_buf, 1, &iobyte, &ioflag, NULL, NULL);

	char * temp = reinterpret_cast<char*>(buffer);

	while (iobyte != 0)
	{
		if (in_packet_size == 0)
		{
			in_packet_size = temp[0];
		}
		if (iobyte + saved_packet_size >= in_packet_size)
		{
			memcpy(buffer + saved_packet_size, temp, in_packet_size - saved_packet_size);
			PacketProccess(buffer);
			temp += in_packet_size - saved_packet_size;
			iobyte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else
		{
			memcpy(buffer + saved_packet_size, temp, iobyte);
			saved_packet_size += iobyte;
			iobyte = 0;
		}
	}
}

void PacketProccess(void * buf) // 실제 패킷 처리
{
	char* temp = reinterpret_cast<char*>(buf);

	switch (temp[1]) {
	case SC_LOGIN_OK: // 로그인 확인 패킷 처리
	{
		sc_packet_login_ok *login_packet = reinterpret_cast<sc_packet_login_ok *>(buf);
		myInfo.id = login_packet->id;
		//myInfo.state = login_packet->state;
		myInfo.state = STATE_FIND_MATCH;
		cout << "서버 접속 완료 \n";
		break;
	}
	case SC_ROOM_INFO: // 방 정보 패킷 처리
	{
		sc_packet_room_info *room_info_packet = reinterpret_cast<sc_packet_room_info *>(buf);
		ROOM_INFO gameRoom{};
		gameRoom.hostID = room_info_packet->hostID;
		gameRoom.number = room_info_packet->roomNumber;
		gameRoom.guestID = room_info_packet->guestID;
		gameRoomVector.emplace_back(gameRoom);
		if (gameRoom.hostID == myInfo.id) {	// 방의 호스트아이디와 자신의 아이디가 같다면 자기가 그 방의 호스트
			myInfo.state = STATE_IN_GAMEROOM;
			myGameInfo.hostID = gameRoom.hostID;
		}
		break;
	}
	case SC_JOIN_ROOM_OK: // 방 입장 확인 패킷 처리
	{
		sc_packet_join_room_ok *join_ok_packet = reinterpret_cast<sc_packet_join_room_ok *>(buf);
		int hostID = join_ok_packet->hostID;
		int joinerID = join_ok_packet->joinID;
		for (auto &room : gameRoomVector) {
			if (room.hostID == hostID) {
				room.guestID = joinerID;
			}
		}
		if (joinerID == myInfo.id) {
			myInfo.state = STATE_PLAYING_GAME;
			myGameInfo.hostID = hostID;
			myGameInfo.guestID = myInfo.id;
		}
		if (hostID == myInfo.id) {
			myInfo.state = STATE_PLAYING_GAME;
			myGameInfo.hostID = myInfo.id;
			myGameInfo.guestID = joinerID;
		}
		break;
	}
	case SC_CHAT: // 채팅 패킷 처리
	{
		sc_packet_chat * chat_packet = reinterpret_cast<sc_packet_chat *>(buf);
		char chat[MAX_STRING];
		memcpy(chat, chat_packet->chat, MAX_STRING);

		char recvtemp[MAX_STRING];

		for (int i = 5; i < MAX_STRING - 1; ++i) {
			recvtemp[i - 5] = chat[i];
		}
		chat[0] = 'O';
		chat[1] = 't';
		chat[2] = 'h';
		chat[3] = 'e';
		chat[4] = 'r';
		chat[5] = ':';
		chat[6] = ' ';

		for (int i = 7; i < MAX_STRING - 1; ++i) {
			chat[i] = recvtemp[i - 7];
		}

		if (Chatting.size() < MAX_LINE) {
			Chatting.emplace_back(chat);
		}
		// 채팅창이 올라가게 함
		else {
			vector<string> strTemp;
			for (int i = 1; i < MAX_LINE; ++i) {
				strTemp.emplace_back(Chatting[i]);
			}
			Chatting.clear();
			for (int i = 0; i < MAX_LINE - 1; ++i) {
				Chatting.emplace_back(strTemp[i]);
			}
			Chatting.emplace_back(chat);
		}
		break;
	}
	case SC_PUT_STONE: //
	{
		sc_packet_put_stone * put_stone_packet = reinterpret_cast<sc_packet_put_stone *>(buf);
		int recvID = put_stone_packet->receiverID;
		int sendID = put_stone_packet->senderID;
		short x = put_stone_packet->x;
		short y = put_stone_packet->y;
		short stoneType = put_stone_packet->stoneType;
		myGameInfo.board[y][x] = stoneType;
		myGameInfo.stoneCount += 1;
		break;
	}
	case SC_GAME_RESULT:
	{
		sc_packet_game_result * game_result_packet = reinterpret_cast<sc_packet_game_result *>(buf);
		int winnerID = game_result_packet->winnerID;
		int loserID = game_result_packet->loserID;
		if (winnerID == myInfo.id) {
			gameResultFlag = RESULT_WIN;
		}
		else if (loserID == myInfo.id) {
			gameResultFlag = RESULT_LOSE;
		}
		myInfo.state = STATE_END_GAME;
		break;
	}
	case SC_REMOVE_ROOM:
	{
		sc_packet_remove_room * remove_room_packet = reinterpret_cast<sc_packet_remove_room *>(buf);
		int hostID = remove_room_packet->hostID;
		int index = 0;
		for (auto &room : gameRoomVector) {
			if (room.hostID == hostID && room.guestID == myInfo.id) { // 삭제된 방의 게스트가 나일경우
				myGameInfo.stoneCount = 0;
				myGameInfo.guestID = -1;
				myGameInfo.hostID = -1;
				myGameInfo.stoneCount = 0;
				for (int i = 0; i < 17; ++i) {
					for (int j = 0; j < 17; ++j) {
						myGameInfo.board[i][j] = STONE_NONE;
					}
				}
				myInfo.state = STATE_FIND_MATCH;
				break;
			}
		}
		for (auto &room : gameRoomVector) {
			if (room.hostID == hostID) {
				gameRoomVector.erase(gameRoomVector.begin() + index);
				break;
			}
			++index;
		}

		break;
	}
	case SC_LEAVE_ROOM:
	{
		sc_packet_leave_room * leave_room_packet = reinterpret_cast<sc_packet_leave_room *>(buf);
		int leaverID = leave_room_packet->leaverID;
		for (auto &room : gameRoomVector) {
			if (room.guestID == leaverID) {
				room.guestID = -1;
			}
		}
		break;
	}

	}
}

void RenderLobbyScene(HDC Backbuffer) { 
	int i = 0;
	for (auto &room : gameRoomVector) {	// 방 리스트
		int roomnumber = room.number;
		int roomhostid = room.hostID;
		int guestid = room.guestID;
		Rectangle(Backbuffer, 10, 10 + (i * 30), 150, 40 + (i * 30));
		TCHAR text[50];
		if (guestid == -1) {
			wsprintf(text, TEXT("[ %d ] id: %d 의 방 (1/2) "), i + 1, roomhostid);
		}
		else {
			wsprintf(text, TEXT("[ %d ] id: %d 의 방 (2/2)"), i + 1, roomhostid);
		}
		
		TextOut(Backbuffer, 10, 15 + (i*30), text, lstrlen(text));
		++i;
	}

	Rectangle(Backbuffer, 400, 400, 550, 500);
	TextOut(Backbuffer, 430, 450, "Make Room", 9);
}


void RenderGameScene(HDC Backbuffer) {

	HBRUSH hBrush = CreateSolidBrush(RGB(250, 150, 30));
	HBRUSH oldBrush = (HBRUSH)SelectObject(Backbuffer, hBrush);

	for (int i = 0; i < 18; ++i) {
		for (int j = 0; j < 18; ++j) {
			Rectangle(Backbuffer, (j * 20), (i * 20), (j + 1) * 20, (i + 1) * 20);
		}
	}
	SelectObject(Backbuffer, oldBrush);
	DeleteObject(hBrush);

	if (gameResultFlag == RESULT_WIN) {
		Rectangle(Backbuffer, 400, 100, 500, 150);
		TextOut(Backbuffer, 418, 110, "YOU WIN!", 8);
	}
	else if(gameResultFlag == RESULT_LOSE) {
		Rectangle(Backbuffer, 400, 100, 500, 150);
		TextOut(Backbuffer, 415, 110, "YOU LOSE..", 10);
	}

}

void DrawStone(HDC Backbuffer)
{
	{
		for (int i = 0; i < 17; ++i) {
			for (int j = 0; j < 17; ++j) {
				// 흰돌
				if (myGameInfo.board[i][j] == STONE_WHITE) {
					HBRUSH hBrush = CreateSolidBrush(RGB(255, 255, 255));
					HBRUSH oldBrush = (HBRUSH)SelectObject(Backbuffer, hBrush);
					Ellipse(Backbuffer, (j * 20) + 10, (i * 20) + 10, (j + 1) * 20 + 10, (i + 1) * 20 + 10);
					SelectObject(Backbuffer, oldBrush);
					DeleteObject(hBrush);
				}
				// 흑돌
				else if (myGameInfo.board[i][j] == STONE_BLACK) {
					HBRUSH hBrush = CreateSolidBrush(RGB(0, 0, 0));
					HBRUSH oldBrush = (HBRUSH)SelectObject(Backbuffer, hBrush);
					Ellipse(Backbuffer, (j * 20) + 10, (i * 20) + 10, (j + 1) * 20 + 10, (i + 1) * 20 + 10);
					SelectObject(Backbuffer, oldBrush);
					DeleteObject(hBrush);
				}
			}
		}
	}
}

void DrawChat(HDC Backbuffer)
{
	HBRUSH hBrush = CreateSolidBrush(RGB(255, 255, 255));
	HBRUSH oldBrush = (HBRUSH)SelectObject(Backbuffer, hBrush);

	Rectangle(Backbuffer, 360, 200, 580, 530);

	SelectObject(Backbuffer, oldBrush);
	DeleteObject(hBrush);
}


// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.


// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_OMOKCLIENT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 응용 프로그램 초기화를 수행합니다:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_OMOKCLIENT));

    MSG msg;

    // 기본 메시지 루프입니다:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_OMOKCLIENT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_OMOKCLIENT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	CW_USEDEFAULT, 0, WINDOWSize_X, WINDOWSize_Y, nullptr, nullptr, hInstance, nullptr);

if (!hWnd)
{
	return FALSE;
}

ShowWindow(hWnd, nCmdShow);
UpdateWindow(hWnd);

return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//	dubble buffer
	HDC hdc, memdc;
	HBITMAP BackBit, oldBackBit;
	PAINTSTRUCT ps;
	static RECT rectView;

	// socket
	static WSADATA WSAData;
	static SOCKET serverSocket;
	static sockaddr_in serverAddr;

	// 마우스 좌표
	int x, y;

	switch (message)
	{
	case WM_CREATE:
	{
		// Socket init
		WSAStartup(MAKEWORD(2, 0), &WSAData);	//  네트워크 기능을 사용하기 위함, 인터넷 표준을 사용하기 위해
		serverSocket = socket(AF_INET, SOCK_STREAM, 0);
		memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_port = htons(SERVER_PORT);
		inet_pton(AF_INET, SERVER_IP, &serverAddr.sin_addr);// ipv4에서 ipv6로 변환
		connect(serverSocket, (SOCKADDR *)&serverAddr, sizeof(serverAddr));

		// wsaasynselect
		WSAAsyncSelect(serverSocket, hWnd, WM_SOCKET, FD_READ || FD_CLOSE);
		recv_buf.len = MAX_BUFFER;
		recv_buf.buf = buffer;  // WSABUF로 buffer주소 지정

		// 더블버퍼링 Rect
		GetClientRect(hWnd, &rectView);

		// 콘솔창
		/*AllocConsole();
		SetConsoleTitle(TEXT("테스트용 콘솔"));
		_tfreopen(_T("CONOUT$"), _T("w"), stdout);
		_tfreopen(_T("CONIN$"), _T("r"), stdin);
		_tfreopen(_T("CONERR$"), _T("w"), stderr);*/

		// chatting
		CreateCaret(hWnd, NULL, 3, 15);
		ShowCaret(hWnd);
		strCount = 0;

		break;
	}

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;

	case WM_CHAR:
		// 백 스페이스 - 글자 지우기
		if (wParam == VK_BACK && strCount != NULL)
			--strCount;
		// 엔터 - 채팅 보내기
		else if (wParam == VK_RETURN) {
			char temp[MAX_STRING];
			for (int i = 0; i < MAX_STRING - 1; ++i) {
				temp[i] = str[i];
			}
			str[0] = 'Y';
			str[1] = 'o';
			str[2] = 'u';
			str[3] = ':';
			str[4] = ' ';
			
			for (int i = 5; i < MAX_STRING - 1; ++i) {
				str[i] = temp[i - 5];
			}
			if (Chatting.size() < MAX_LINE)
			{
				Chatting.emplace_back(str);
			}
			// 채팅창이 올라가게 함
			else {
				vector<string> strTemp;
				for (int i = 1; i < MAX_LINE; ++i) {
					strTemp.emplace_back(Chatting[i]);
				}
				Chatting.clear();
				for (int i = 0; i < MAX_LINE - 1; ++i) {
					Chatting.emplace_back(strTemp[i]);
				}
				Chatting.emplace_back(str);
			}
				cs_packet_chat packet;
				packet.type = CS_CHAT;
				packet.senderID = myInfo.id;
				for (auto& room : gameRoomVector) {
					if (room.guestID == myInfo.id) {
						packet.receiverID = room.hostID;
						break;
					}
					else if (room.hostID == myInfo.id) {
						packet.receiverID = room.guestID;
						break;
					}
				}
				packet.size = sizeof(packet);
				memcpy(packet.chat, str, sizeof(str));
				send(serverSocket, (char*)&packet, sizeof(packet), 0);

			strCount = 0;
		}
		// 그 외 - 입력
		else {
			if (strCount < MAX_STRING - 8) {
				str[strCount] = wParam;
				++strCount;
			}
		}
		str[strCount] = '\0';

		InvalidateRect(hWnd, NULL, TRUE);
		break;

	case WM_PAINT:
	{
		// dubble buffer
		hdc = BeginPaint(hWnd, &ps);
		memdc = CreateCompatibleDC(hdc);
		BackBit = CreateCompatibleBitmap(hdc, rectView.right, rectView.bottom);
		oldBackBit = (HBITMAP)SelectObject(memdc, BackBit);

		if (myGameInfo.hostID == myInfo.id) {
			if (myGameInfo.stoneCount % 2 == 1) DrawTurn = TRUE;
			else DrawTurn = FALSE;
		}
		else {
			if (myGameInfo.stoneCount % 2 == 0) DrawTurn = TRUE;
			else DrawTurn = FALSE;
		}

		if (myInfo.state == STATE_FIND_MATCH) { // 메인로비
			RenderLobbyScene(memdc);
		}
		else if (myInfo.state == STATE_IN_GAMEROOM || myInfo.state == STATE_PLAYING_GAME 
			|| myInfo.state == STATE_END_GAME) { // 방입장
			int num = 0;
			RenderGameScene(memdc);
			DrawStone(memdc);
			DrawChat(memdc);
			TextOut(memdc, 365, 500, str, strlen(str));
			// 채팅창
			for (auto iter = Chatting.begin(); iter != Chatting.end(); ++iter) {
				char temp[MAX_STRING];
				if (num > MAX_LINE) {
					--num;
					for (int j = 0; j < MAX_LINE - 1; ++j)
						temp[j] = NULL;
				}
				strcpy(temp, (*iter).c_str());
				TextOut(memdc, 370, 210 + (num * 20), temp, strlen(temp));
				++num;
			}
			// 차례
			if (DrawTurn)
				TextOut(memdc, 150, 400, "Your Turn", 9);
		}

		// 백버퍼 불러오기
		BitBlt(hdc, 0, 0, rectView.right, rectView.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, oldBackBit));
		DeleteDC(memdc);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다...
		EndPaint(hWnd, &ps);
	}
	break;


	case WM_LBUTTONDOWN:
		if (myInfo.state == STATE_FIND_MATCH) { 
			x = LOWORD(lParam);
			y = HIWORD(lParam) - 10;
			y /= 30;
			if (x > 10 && x < 150 && y < gameRoomVector.size()) {
				cs_packet_join_room packet;
				packet.type = CS_JOIN_ROOM;
				packet.hostID = gameRoomVector[y].hostID;
				packet.joinID = myInfo.id;
				packet.size = sizeof(packet);
				send(serverSocket, (char*)&packet, sizeof(packet), 0);
			}
			y *= 30;
			//400, 400, 550, 500
			if (x > 400 && y > 400 - 10 && x < 550 && y < 500 - 10) {
				cs_packet_make_room packet;
				packet.type = CS_MAKE_ROOM;
				packet.id = myInfo.id;
				packet.size = sizeof(packet);
				send(serverSocket, (char*)&packet, sizeof(packet), 0);
			}
		}
		else if (myInfo.state == STATE_PLAYING_GAME) {
			x = LOWORD(lParam) - 10;
			y = HIWORD(lParam) - 10;
			x /= 20;
			y /= 20;
			short count = myGameInfo.stoneCount;
			if (myGameInfo.hostID == myInfo.id) { // 호스트일때
				if (x < 17 && y < 17 && myGameInfo.board[y][x] == NULL && (count%2)==1) {
					cs_packet_put_stone packet;
					packet.type = CS_PUT_STONE;
					packet.senderID = myInfo.id;
					packet.stoneType = STONE_WHITE;
					packet.receiverID = myGameInfo.guestID;
					packet.x = x;
					packet.y = y;
					packet.size = sizeof(packet);
					send(serverSocket, (char*)&packet, sizeof(packet), 0);
					DrawTurn = FALSE;
				}
			}
			else if (myGameInfo.guestID == myInfo.id) { // 게스트일때
				if (x < 17 && y < 17 && myGameInfo.board[y][x] == NULL && (count%2)==0) {
					cs_packet_put_stone packet;
					packet.type = CS_PUT_STONE;
					packet.senderID = myInfo.id;
					packet.stoneType = STONE_BLACK;
					packet.receiverID = myGameInfo.hostID;
					packet.x = x;
					packet.y = y;
					packet.size = sizeof(packet);
					send(serverSocket, (char*)&packet, sizeof(packet), 0);
					DrawTurn = FALSE;
				}
			}
		}
		InvalidateRgn(hWnd, NULL, FALSE);
		break;

	case WM_SOCKET:
		if (WSAGETSELECTERROR(lParam)) {
			closesocket((SOCKET)wParam);
			break;
		}
		switch (WSAGETSELECTEVENT(lParam)) {
		case FD_READ:
			ReadBuffer((SOCKET)wParam);
			break;
		}
		InvalidateRgn(hWnd, NULL, FALSE);
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
